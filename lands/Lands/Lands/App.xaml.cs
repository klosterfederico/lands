﻿using Lands.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Lands
{
    public partial class App : Application
    {
        #region Constructors
        public App()
        {
            InitializeComponent();

            //MainPage = new MainPage();
            this.MainPage = new NavigationPage(new LoginPage());
        }
        #endregion

        #region Methods
        protected override void OnStart()
        {
            // Handle when your app starts
            //Se ejecuta cuando la aplicación se inicia.
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
            //Cuando estamos en la aplicación y abrimos otra, es decir que esta queda en segundo plano, suspendida.
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
            //Cuando volvemos ingresar a la aplicación que estaba en estado suspendida.
        }
        #endregion
    }
}
